﻿

Public Class searchForm

    Dim intID As Integer

    Private Sub dvgSearch_Focus(sender As Object, e As EventArgs) Handles dvgSearch.GotFocus

    End Sub


    Private Sub searchForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'MusicDBWorkingDataSet.Songs' table. You can move, or remove it, as needed.
        Me.SongsTableAdapter.Fill(Me.MusicDBWorkingDataSet.Songs)

        'default to artist
        radArtist.Checked = True

    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        Dim dv As New DataView
        dv.Table = MusicDBWorkingDataSet.Songs

        'if there is no text then fill with value
        If Trim(txtSearch.Text) = "" Then
            dvgSearch.DataSource = dv
            Exit Sub
        End If

        'decide what to search for
        If radSong.Checked Then
            dv.RowFilter = "[SongTitle] Like '" & txtSearch.Text & "%'"
        ElseIf radAlbum.Checked Then
            dv.RowFilter = "[Album] Like '" & txtSearch.Text & "%'"
        ElseIf radArtist.Checked Then
            dv.RowFilter = "[Artist] Like '" & txtSearch.Text & "%'"
        End If

        'update grid
        dvgSearch.DataSource = dv

    End Sub

    Private Sub dvgSearch_CellClick(ByVal sender As System.Object,
                                    ByVal e As DataGridViewCellEventArgs) _
                                    Handles dvgSearch.CellClick


        intID = dvgSearch.Rows(e.RowIndex).Cells(0).Value

    End Sub

    Private Sub btnPlay_Click(sender As Object, e As EventArgs) Handles btnPlay.Click
        Me.Close()
        If Form1.pboPlay.Visible = True Then
            Form1.pboPlay.Visible = False
            Form1.pboStop.Visible = True
        End If

        Form1.play(intID)

    End Sub
End Class