﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.pBarRunTime = New System.Windows.Forms.ProgressBar()
        Me.MusicDBWorkingDataSet = New FinalProject.MusicDBWorkingDataSet()
        Me.SongsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SongsTableAdapter = New FinalProject.MusicDBWorkingDataSetTableAdapters.SongsTableAdapter()
        Me.TableAdapterManager = New FinalProject.MusicDBWorkingDataSetTableAdapters.TableAdapterManager()
        Me.SongsDataGridView = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SongTitleDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ArtistDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AlbumDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.lblTimePlayed = New System.Windows.Forms.Label()
        Me.lblStaticTime = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.btnUpload = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblArtist = New System.Windows.Forms.Label()
        Me.lblSong = New System.Windows.Forms.Label()
        Me.mnuStrp = New System.Windows.Forms.MenuStrip()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.PrintPreviewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.lblShuffle = New System.Windows.Forms.Label()
        Me.lblRepeat = New System.Windows.Forms.Label()
        Me.chkRepeat = New System.Windows.Forms.CheckBox()
        Me.chkShuffle = New System.Windows.Forms.CheckBox()
        Me.pboForward = New System.Windows.Forms.PictureBox()
        Me.pboPlay = New System.Windows.Forms.PictureBox()
        Me.pboStop = New System.Windows.Forms.PictureBox()
        Me.pboRwd = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.MusicDBWorkingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SongsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SongsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnuStrp.SuspendLayout()
        CType(Me.pboForward, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboPlay, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboStop, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pboRwd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pBarRunTime
        '
        Me.pBarRunTime.Location = New System.Drawing.Point(86, 381)
        Me.pBarRunTime.Name = "pBarRunTime"
        Me.pBarRunTime.Size = New System.Drawing.Size(455, 23)
        Me.pBarRunTime.TabIndex = 0
        '
        'MusicDBWorkingDataSet
        '
        Me.MusicDBWorkingDataSet.DataSetName = "MusicDBWorkingDataSet"
        Me.MusicDBWorkingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SongsBindingSource
        '
        Me.SongsBindingSource.DataMember = "Songs"
        Me.SongsBindingSource.DataSource = Me.MusicDBWorkingDataSet
        '
        'SongsTableAdapter
        '
        Me.SongsTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.SongsTableAdapter = Me.SongsTableAdapter
        Me.TableAdapterManager.UpdateOrder = FinalProject.MusicDBWorkingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SongsDataGridView
        '
        Me.SongsDataGridView.AllowUserToAddRows = False
        Me.SongsDataGridView.AllowUserToDeleteRows = False
        Me.SongsDataGridView.AutoGenerateColumns = False
        Me.SongsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.SongsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SongsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.SongTitleDataGridViewTextBoxColumn, Me.ArtistDataGridViewTextBoxColumn, Me.AlbumDataGridViewTextBoxColumn})
        Me.SongsDataGridView.DataSource = Me.SongsBindingSource
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.SongsDataGridView.DefaultCellStyle = DataGridViewCellStyle1
        Me.SongsDataGridView.Location = New System.Drawing.Point(86, 128)
        Me.SongsDataGridView.Name = "SongsDataGridView"
        Me.SongsDataGridView.ReadOnly = True
        Me.SongsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.SongsDataGridView.Size = New System.Drawing.Size(455, 220)
        Me.SongsDataGridView.TabIndex = 8
        '
        'ID
        '
        Me.ID.DataPropertyName = "ID"
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        '
        'SongTitleDataGridViewTextBoxColumn
        '
        Me.SongTitleDataGridViewTextBoxColumn.DataPropertyName = "SongTitle"
        Me.SongTitleDataGridViewTextBoxColumn.HeaderText = "SongTitle"
        Me.SongTitleDataGridViewTextBoxColumn.Name = "SongTitleDataGridViewTextBoxColumn"
        Me.SongTitleDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ArtistDataGridViewTextBoxColumn
        '
        Me.ArtistDataGridViewTextBoxColumn.DataPropertyName = "Artist"
        Me.ArtistDataGridViewTextBoxColumn.HeaderText = "Artist"
        Me.ArtistDataGridViewTextBoxColumn.Name = "ArtistDataGridViewTextBoxColumn"
        Me.ArtistDataGridViewTextBoxColumn.ReadOnly = True
        '
        'AlbumDataGridViewTextBoxColumn
        '
        Me.AlbumDataGridViewTextBoxColumn.DataPropertyName = "Album"
        Me.AlbumDataGridViewTextBoxColumn.HeaderText = "Album"
        Me.AlbumDataGridViewTextBoxColumn.Name = "AlbumDataGridViewTextBoxColumn"
        Me.AlbumDataGridViewTextBoxColumn.ReadOnly = True
        '
        'btnSearch
        '
        Me.btnSearch.BackColor = System.Drawing.Color.LimeGreen
        Me.btnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch.Location = New System.Drawing.Point(86, 74)
        Me.btnSearch.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(103, 32)
        Me.btnSearch.TabIndex = 9
        Me.btnSearch.Text = "&Search"
        Me.btnSearch.UseVisualStyleBackColor = False
        '
        'lblTimePlayed
        '
        Me.lblTimePlayed.AutoSize = True
        Me.lblTimePlayed.BackColor = System.Drawing.Color.Transparent
        Me.lblTimePlayed.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTimePlayed.ForeColor = System.Drawing.Color.DarkRed
        Me.lblTimePlayed.Location = New System.Drawing.Point(34, 381)
        Me.lblTimePlayed.Name = "lblTimePlayed"
        Me.lblTimePlayed.Size = New System.Drawing.Size(0, 20)
        Me.lblTimePlayed.TabIndex = 10
        '
        'lblStaticTime
        '
        Me.lblStaticTime.AutoSize = True
        Me.lblStaticTime.BackColor = System.Drawing.Color.Transparent
        Me.lblStaticTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStaticTime.ForeColor = System.Drawing.Color.DarkRed
        Me.lblStaticTime.Location = New System.Drawing.Point(568, 384)
        Me.lblStaticTime.Name = "lblStaticTime"
        Me.lblStaticTime.Size = New System.Drawing.Size(0, 20)
        Me.lblStaticTime.TabIndex = 11
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'btnUpload
        '
        Me.btnUpload.BackColor = System.Drawing.Color.LimeGreen
        Me.btnUpload.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpload.Location = New System.Drawing.Point(454, 75)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(87, 31)
        Me.btnUpload.TabIndex = 12
        Me.btnUpload.Text = "&Upload"
        Me.btnUpload.UseVisualStyleBackColor = False
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.LimeGreen
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(270, 75)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(87, 31)
        Me.btnExit.TabIndex = 19
        Me.btnExit.Text = "E&xit"
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(84, 351)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 18)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Now Playing:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(360, 351)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 18)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Artist:"
        '
        'lblArtist
        '
        Me.lblArtist.AutoSize = True
        Me.lblArtist.BackColor = System.Drawing.Color.Transparent
        Me.lblArtist.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArtist.Location = New System.Drawing.Point(416, 351)
        Me.lblArtist.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblArtist.Name = "lblArtist"
        Me.lblArtist.Size = New System.Drawing.Size(0, 18)
        Me.lblArtist.TabIndex = 23
        '
        'lblSong
        '
        Me.lblSong.AutoSize = True
        Me.lblSong.BackColor = System.Drawing.Color.Transparent
        Me.lblSong.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSong.Location = New System.Drawing.Point(194, 351)
        Me.lblSong.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblSong.Name = "lblSong"
        Me.lblSong.Size = New System.Drawing.Size(0, 18)
        Me.lblSong.TabIndex = 24
        '
        'mnuStrp
        '
        Me.mnuStrp.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem, Me.HelpToolStripMenuItem, Me.PrintToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.mnuStrp.Location = New System.Drawing.Point(0, 0)
        Me.mnuStrp.Name = "mnuStrp"
        Me.mnuStrp.Size = New System.Drawing.Size(644, 24)
        Me.mnuStrp.TabIndex = 25
        Me.mnuStrp.Text = "MenuStrip1"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.AboutToolStripMenuItem.Text = "&About"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "&Help"
        '
        'PrintToolStripMenuItem
        '
        Me.PrintToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrintToolStripMenuItem1, Me.ToolStripSeparator1, Me.PrintPreviewToolStripMenuItem})
        Me.PrintToolStripMenuItem.Name = "PrintToolStripMenuItem"
        Me.PrintToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.PrintToolStripMenuItem.Text = "&File"
        '
        'PrintToolStripMenuItem1
        '
        Me.PrintToolStripMenuItem1.Name = "PrintToolStripMenuItem1"
        Me.PrintToolStripMenuItem1.Size = New System.Drawing.Size(143, 22)
        Me.PrintToolStripMenuItem1.Text = "&Print"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(140, 6)
        '
        'PrintPreviewToolStripMenuItem
        '
        Me.PrintPreviewToolStripMenuItem.Name = "PrintPreviewToolStripMenuItem"
        Me.PrintPreviewToolStripMenuItem.Size = New System.Drawing.Size(143, 22)
        Me.PrintPreviewToolStripMenuItem.Text = "Print Pre&view"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'PrintDocument1
        '
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'lblShuffle
        '
        Me.lblShuffle.AutoSize = True
        Me.lblShuffle.BackColor = System.Drawing.Color.Transparent
        Me.lblShuffle.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShuffle.Location = New System.Drawing.Point(451, 471)
        Me.lblShuffle.Name = "lblShuffle"
        Me.lblShuffle.Size = New System.Drawing.Size(87, 18)
        Me.lblShuffle.TabIndex = 27
        Me.lblShuffle.Text = "Shuffle On"
        Me.lblShuffle.Visible = False
        '
        'lblRepeat
        '
        Me.lblRepeat.AutoSize = True
        Me.lblRepeat.BackColor = System.Drawing.Color.Transparent
        Me.lblRepeat.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRepeat.Location = New System.Drawing.Point(83, 471)
        Me.lblRepeat.Name = "lblRepeat"
        Me.lblRepeat.Size = New System.Drawing.Size(88, 18)
        Me.lblRepeat.TabIndex = 29
        Me.lblRepeat.Text = "Repeat On"
        Me.lblRepeat.Visible = False
        '
        'chkRepeat
        '
        Me.chkRepeat.AutoSize = True
        Me.chkRepeat.BackColor = System.Drawing.Color.Transparent
        Me.chkRepeat.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRepeat.Location = New System.Drawing.Point(86, 432)
        Me.chkRepeat.Name = "chkRepeat"
        Me.chkRepeat.Size = New System.Drawing.Size(80, 22)
        Me.chkRepeat.TabIndex = 30
        Me.chkRepeat.Text = "Repeat"
        Me.chkRepeat.UseVisualStyleBackColor = False
        '
        'chkShuffle
        '
        Me.chkShuffle.AutoSize = True
        Me.chkShuffle.BackColor = System.Drawing.Color.Transparent
        Me.chkShuffle.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShuffle.Location = New System.Drawing.Point(454, 432)
        Me.chkShuffle.Name = "chkShuffle"
        Me.chkShuffle.Size = New System.Drawing.Size(79, 22)
        Me.chkShuffle.TabIndex = 31
        Me.chkShuffle.Text = "Shuffle"
        Me.chkShuffle.UseVisualStyleBackColor = False
        '
        'pboForward
        '
        Me.pboForward.Image = CType(resources.GetObject("pboForward.Image"), System.Drawing.Image)
        Me.pboForward.Location = New System.Drawing.Point(363, 428)
        Me.pboForward.Name = "pboForward"
        Me.pboForward.Size = New System.Drawing.Size(70, 68)
        Me.pboForward.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pboForward.TabIndex = 13
        Me.pboForward.TabStop = False
        '
        'pboPlay
        '
        Me.pboPlay.ErrorImage = CType(resources.GetObject("pboPlay.ErrorImage"), System.Drawing.Image)
        Me.pboPlay.Image = CType(resources.GetObject("pboPlay.Image"), System.Drawing.Image)
        Me.pboPlay.Location = New System.Drawing.Point(287, 421)
        Me.pboPlay.Name = "pboPlay"
        Me.pboPlay.Size = New System.Drawing.Size(70, 77)
        Me.pboPlay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pboPlay.TabIndex = 17
        Me.pboPlay.TabStop = False
        '
        'pboStop
        '
        Me.pboStop.Image = CType(resources.GetObject("pboStop.Image"), System.Drawing.Image)
        Me.pboStop.Location = New System.Drawing.Point(287, 428)
        Me.pboStop.Name = "pboStop"
        Me.pboStop.Size = New System.Drawing.Size(70, 67)
        Me.pboStop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pboStop.TabIndex = 16
        Me.pboStop.TabStop = False
        Me.pboStop.Visible = False
        '
        'pboRwd
        '
        Me.pboRwd.BackColor = System.Drawing.Color.Transparent
        Me.pboRwd.Image = CType(resources.GetObject("pboRwd.Image"), System.Drawing.Image)
        Me.pboRwd.Location = New System.Drawing.Point(207, 428)
        Me.pboRwd.Name = "pboRwd"
        Me.pboRwd.Size = New System.Drawing.Size(70, 67)
        Me.pboRwd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pboRwd.TabIndex = 18
        Me.pboRwd.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Location = New System.Drawing.Point(196, 419)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(237, 84)
        Me.PictureBox1.TabIndex = 20
        Me.PictureBox1.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Yellow
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(644, 524)
        Me.Controls.Add(Me.chkShuffle)
        Me.Controls.Add(Me.chkRepeat)
        Me.Controls.Add(Me.lblRepeat)
        Me.Controls.Add(Me.lblShuffle)
        Me.Controls.Add(Me.mnuStrp)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.pboForward)
        Me.Controls.Add(Me.pboPlay)
        Me.Controls.Add(Me.pboStop)
        Me.Controls.Add(Me.pboRwd)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnUpload)
        Me.Controls.Add(Me.lblStaticTime)
        Me.Controls.Add(Me.lblTimePlayed)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.pBarRunTime)
        Me.Controls.Add(Me.lblSong)
        Me.Controls.Add(Me.lblArtist)
        Me.Controls.Add(Me.SongsDataGridView)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "some sweet music player app"
        CType(Me.MusicDBWorkingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SongsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SongsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnuStrp.ResumeLayout(False)
        Me.mnuStrp.PerformLayout()
        CType(Me.pboForward, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboPlay, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboStop, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pboRwd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pBarRunTime As ProgressBar
    Friend WithEvents pboForward As PictureBox
    Friend WithEvents MusicDBWorkingDataSet As MusicDBWorkingDataSet
    Friend WithEvents SongsBindingSource As BindingSource
    Friend WithEvents SongsTableAdapter As MusicDBWorkingDataSetTableAdapters.SongsTableAdapter
    Friend WithEvents TableAdapterManager As MusicDBWorkingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents SongsDataGridView As DataGridView
    Friend WithEvents btnSearch As Button
    Friend WithEvents lblTimePlayed As Label
    Friend WithEvents lblStaticTime As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents btnUpload As Button
    Friend WithEvents pboStop As PictureBox
    Friend WithEvents pboPlay As PictureBox
    Friend WithEvents pboRwd As PictureBox
    Friend WithEvents btnExit As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lblArtist As Label
    Friend WithEvents lblSong As Label
    Friend WithEvents mnuStrp As MenuStrip
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents PrintPreviewToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
    Friend WithEvents PrintPreviewDialog1 As PrintPreviewDialog
    Friend WithEvents lblShuffle As Label
    Friend WithEvents lblRepeat As Label
    Friend WithEvents ID As DataGridViewTextBoxColumn
    Friend WithEvents SongTitleDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ArtistDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents AlbumDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents chkRepeat As CheckBox
    Friend WithEvents chkShuffle As CheckBox
End Class
