﻿Public Class upload
    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click
        Dim myFileDlog As New OpenFileDialog()
        Dim strFile As String = ""

        'ensure that values were entered for upload
        If txtArtist.Text.ToString = "" Or txtTitle.Text.ToString = "" Or TxtRunTime.Text.ToString = "" Or txtAlbum.Text.ToString = "" Then

            MsgBox("Not all fields were properly entered.")
            Exit Sub

        End If

        'look for files in the c drive
        myFileDlog.InitialDirectory = "c:\"

        'specifies what type of data files to look for
        myFileDlog.Filter = "All Files (*.*)|*.*" &
            "|Wav (*.wav)|*.wav"

        'specifies which data type is focused on start up
        myFileDlog.FilterIndex = 2

        'Gets or sets a value indicating whether the dialog box restores the current directory before closing.
        myFileDlog.RestoreDirectory = True

        MsgBox("You will now be promted to browse for the audio file you wish to upload")

        'seperates message outputs for files found or not found
        If myFileDlog.ShowDialog() =
            DialogResult.OK Then
            If Dir(myFileDlog.FileName) <> "" Then
                strFile = myFileDlog.FileName
                MsgBox("File Exists: " &
                       myFileDlog.FileName,
                       MsgBoxStyle.Information)

                copyFileToResources(strFile)

            Else
                MsgBox("File Not Found",
                       MsgBoxStyle.Critical)
            End If
        End If




    End Sub

    'copies the file into the resources directory                   'APB 20150525
    Private Sub copyFileToResources(ByVal strFile As String)
        'Dim strWavName As String
        'Dim intSlash As Integer
        'Dim intDot As Integer
        Dim strProjectPath As String
        Dim strFileWithExtension As String
        Dim myFileDlog As New OpenFileDialog()
        Dim picFile As String = ""

        ''get name of wav from path                                                    
        'strWavName = StrReverse(strFile)                            'reverse the path so first instance of \ will be found following the wav file
        'intSlash = InStr(strWavName, "\", CompareMethod.Text)       'get the index of the \
        'strWavName = Mid(strWavName, 1, intSlash - 1)               'pull the XXX.wav out of the path
        'strWavName = StrReverse(strWavName)                         'save the value of XXX.wav after putting the string back in order
        'strFileWithExtension = strWavName
        'intDot = InStr(strWavName, ".", CompareMethod.Text)         'find the index of the .
        'strWavName = Mid(strWavName, 1, intDot - 1)                 'pull the name of the song out so the copy in the music folder will match

        'IF THE USER INPUTS A DIFFERENT SONG TITLE IN THE FORM
        'THEN THE PROGRAM WILL NOT BE ABLE TO MATCH THE DATASET RECORD
        'WITH THE FILE WE COULD FIX THIS LATER, TIME PERMITTING

        strFileWithExtension = txtTitle.Text & ".wav"
        'copy the file into the resources folder music
        strProjectPath = AppDomain.CurrentDomain.BaseDirectory              'Pull the directory of the project
        strProjectPath = strProjectPath.Replace("\bin\Debug\", "\Music\")   'Set the path to the actual folder we want


        'copy the file and if the file exists, the user can choose weather to overwrite
        My.Computer.FileSystem.CopyFile(strFile, strProjectPath & strFileWithExtension,
                                        Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
                                        Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)

        MsgBox("Copy was succesful")

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'test to see if the user wants to upload a picture
        If cboArt.Checked Then

            'look for files in the c drive
            myFileDlog.InitialDirectory = "c:\"

            'specifies what type of data files to look for
            myFileDlog.Filter = "All Files (*.*)|*.*" &
                "|Jpg (*.jpg)|*.jpg"

            'specifies which data type is focused on start up
            myFileDlog.FilterIndex = 2

            'Gets or sets a value indicating whether the dialog box restores the current directory before closing.
            myFileDlog.RestoreDirectory = True

            MsgBox("You will now be promted to browse for the image to upload")

            If myFileDlog.ShowDialog() =
            DialogResult.OK Then
                If Dir(myFileDlog.FileName) <> "" Then
                    picFile = myFileDlog.FileName
                    MsgBox("File Exists: " &
                           myFileDlog.FileName,
                           MsgBoxStyle.Information)


                Else
                    MsgBox("File Not Found",
                           MsgBoxStyle.Critical)
                End If
            End If

            'copy the picture into resources
            strFileWithExtension = txtArtist.Text & ".jpg"
            'copy the file into the resources folder music
            strProjectPath = AppDomain.CurrentDomain.BaseDirectory              'Pull the directory of the project
            strProjectPath = strProjectPath.Replace("\bin\Debug\", "\Images\")   'Set the path to the actual folder we want


            'copy the file and if the file exists, the user can choose weather to overwrite
            My.Computer.FileSystem.CopyFile(picFile, strProjectPath & strFileWithExtension,
                                            Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
                                            Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)

            MsgBox("Image Copy was succesful")

        End If
        'get the data from to update the dataset and database
        updateData()
    End Sub

    Private Sub updateData()                                            'APB 20160525
        Dim strArtist As String
        Dim strSong As String
        Dim intRunTime As Integer
        Dim intID As Integer
        Dim strAlbum As String

        'get values from form
        strArtist = txtArtist.Text.ToString
        strSong = txtTitle.Text.ToString
        Integer.TryParse(TxtRunTime.Text, intRunTime)
        strAlbum = txtAlbum.Text.ToString

        'find the id of the song in the dataset
        'I changed the ID column in the DB to be a number not autonumber
        'so we have to make sure that they are in sequential order 


        'assings an ID 1 higher than the highest in the DB
        intID = (Aggregate id In Form1.MusicDBWorkingDataSet.Songs
                    Into Max(id.ID)) + 1

        Me.Close()
        'Sends the data to the form so that it can alter the dataset and database
        Form1.updateData(strSong, strArtist, intID, intRunTime, strAlbum)


    End Sub
End Class