﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class searchForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.SongsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MusicDBWorkingDataSet = New FinalProject.MusicDBWorkingDataSet()
        Me.SongsTableAdapter = New FinalProject.MusicDBWorkingDataSetTableAdapters.SongsTableAdapter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.radAlbum = New System.Windows.Forms.RadioButton()
        Me.radSong = New System.Windows.Forms.RadioButton()
        Me.radArtist = New System.Windows.Forms.RadioButton()
        Me.btnPlay = New System.Windows.Forms.Button()
        Me.AlbumDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ArtistDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SongTitleDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dvgSearch = New System.Windows.Forms.DataGridView()
        CType(Me.SongsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MusicDBWorkingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dvgSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtSearch
        '
        Me.txtSearch.Location = New System.Drawing.Point(9, 26)
        Me.txtSearch.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(76, 20)
        Me.txtSearch.TabIndex = 2
        '
        'SongsBindingSource
        '
        Me.SongsBindingSource.DataMember = "Songs"
        Me.SongsBindingSource.DataSource = Me.MusicDBWorkingDataSet
        '
        'MusicDBWorkingDataSet
        '
        Me.MusicDBWorkingDataSet.DataSetName = "MusicDBWorkingDataSet"
        Me.MusicDBWorkingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SongsTableAdapter
        '
        Me.SongsTableAdapter.ClearBeforeFill = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.radAlbum)
        Me.GroupBox1.Controls.Add(Me.radSong)
        Me.GroupBox1.Controls.Add(Me.radArtist)
        Me.GroupBox1.Location = New System.Drawing.Point(104, 10)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.GroupBox1.Size = New System.Drawing.Size(176, 44)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Search"
        '
        'radAlbum
        '
        Me.radAlbum.AutoSize = True
        Me.radAlbum.Location = New System.Drawing.Point(106, 18)
        Me.radAlbum.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.radAlbum.Name = "radAlbum"
        Me.radAlbum.Size = New System.Drawing.Size(54, 17)
        Me.radAlbum.TabIndex = 2
        Me.radAlbum.TabStop = True
        Me.radAlbum.Text = "Album"
        Me.radAlbum.UseVisualStyleBackColor = True
        '
        'radSong
        '
        Me.radSong.AutoSize = True
        Me.radSong.Location = New System.Drawing.Point(56, 17)
        Me.radSong.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.radSong.Name = "radSong"
        Me.radSong.Size = New System.Drawing.Size(50, 17)
        Me.radSong.TabIndex = 1
        Me.radSong.TabStop = True
        Me.radSong.Text = "Song"
        Me.radSong.UseVisualStyleBackColor = True
        '
        'radArtist
        '
        Me.radArtist.AutoSize = True
        Me.radArtist.Location = New System.Drawing.Point(5, 18)
        Me.radArtist.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.radArtist.Name = "radArtist"
        Me.radArtist.Size = New System.Drawing.Size(48, 17)
        Me.radArtist.TabIndex = 0
        Me.radArtist.TabStop = True
        Me.radArtist.Text = "Artist"
        Me.radArtist.UseVisualStyleBackColor = True
        '
        'btnPlay
        '
        Me.btnPlay.Location = New System.Drawing.Point(292, 22)
        Me.btnPlay.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnPlay.Name = "btnPlay"
        Me.btnPlay.Size = New System.Drawing.Size(86, 26)
        Me.btnPlay.TabIndex = 7
        Me.btnPlay.Text = "Play"
        Me.btnPlay.UseVisualStyleBackColor = True
        '
        'AlbumDataGridViewTextBoxColumn
        '
        Me.AlbumDataGridViewTextBoxColumn.DataPropertyName = "Album"
        Me.AlbumDataGridViewTextBoxColumn.HeaderText = "Album"
        Me.AlbumDataGridViewTextBoxColumn.Name = "AlbumDataGridViewTextBoxColumn"
        Me.AlbumDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ArtistDataGridViewTextBoxColumn
        '
        Me.ArtistDataGridViewTextBoxColumn.DataPropertyName = "Artist"
        Me.ArtistDataGridViewTextBoxColumn.HeaderText = "Artist"
        Me.ArtistDataGridViewTextBoxColumn.Name = "ArtistDataGridViewTextBoxColumn"
        Me.ArtistDataGridViewTextBoxColumn.ReadOnly = True
        '
        'SongTitleDataGridViewTextBoxColumn
        '
        Me.SongTitleDataGridViewTextBoxColumn.DataPropertyName = "SongTitle"
        Me.SongTitleDataGridViewTextBoxColumn.HeaderText = "SongTitle"
        Me.SongTitleDataGridViewTextBoxColumn.Name = "SongTitleDataGridViewTextBoxColumn"
        Me.SongTitleDataGridViewTextBoxColumn.ReadOnly = True
        '
        'IDDataGridViewTextBoxColumn
        '
        Me.IDDataGridViewTextBoxColumn.DataPropertyName = "ID"
        Me.IDDataGridViewTextBoxColumn.HeaderText = "ID"
        Me.IDDataGridViewTextBoxColumn.Name = "IDDataGridViewTextBoxColumn"
        Me.IDDataGridViewTextBoxColumn.ReadOnly = True
        '
        'dvgSearch
        '
        Me.dvgSearch.AllowUserToAddRows = False
        Me.dvgSearch.AllowUserToDeleteRows = False
        Me.dvgSearch.AllowUserToOrderColumns = True
        Me.dvgSearch.AutoGenerateColumns = False
        Me.dvgSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dvgSearch.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDDataGridViewTextBoxColumn, Me.SongTitleDataGridViewTextBoxColumn, Me.ArtistDataGridViewTextBoxColumn, Me.AlbumDataGridViewTextBoxColumn})
        Me.dvgSearch.DataSource = Me.SongsBindingSource
        Me.dvgSearch.Location = New System.Drawing.Point(16, 67)
        Me.dvgSearch.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.dvgSearch.Name = "dvgSearch"
        Me.dvgSearch.ReadOnly = True
        Me.dvgSearch.RowTemplate.Height = 24
        Me.dvgSearch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dvgSearch.Size = New System.Drawing.Size(362, 204)
        Me.dvgSearch.TabIndex = 5
        '
        'searchForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Yellow
        Me.ClientSize = New System.Drawing.Size(407, 286)
        Me.Controls.Add(Me.btnPlay)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dvgSearch)
        Me.Controls.Add(Me.txtSearch)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "searchForm"
        Me.Text = "searchForm"
        CType(Me.SongsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MusicDBWorkingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dvgSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtSearch As TextBox
    Friend WithEvents MusicDBWorkingDataSet As MusicDBWorkingDataSet
    Friend WithEvents SongsBindingSource As BindingSource
    Friend WithEvents SongsTableAdapter As MusicDBWorkingDataSetTableAdapters.SongsTableAdapter
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents radAlbum As RadioButton
    Friend WithEvents radSong As RadioButton
    Friend WithEvents radArtist As RadioButton
    Friend WithEvents btnPlay As Button
    Friend WithEvents AlbumDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ArtistDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SongTitleDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents IDDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents dvgSearch As DataGridView
End Class
