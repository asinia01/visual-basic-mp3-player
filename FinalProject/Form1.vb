﻿'Program: Desktop music player app
'Programmers: Andrew M. Siniarski, Alex Brouwer, Steve Brittain
' the dude abides
' winter is coming
'Date: May 2016


Public Class Form1
    'variable for the id of the song
    Private intIDClick As Integer
    'arrays for has been played songs
    Private strArtistArray(20) As String
    Private strAlbumArray(20) As String
    Private strSongArray(20) As String
    Dim intArrayAccum As Integer = 0
    'variables for playing stuff
    Private strTime As String
    Private intSeconds As Integer
    'variable for print
    Private pageCount As Integer = 1


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'MusicDBWorkingDataSet.Songs' table. You can move, or remove it, as needed.
        Me.SongsTableAdapter.Fill(Me.MusicDBWorkingDataSet.Songs)

    End Sub

    Private Sub musicClick_CellClick(ByVal sender As System.Object,
                                    ByVal e As DataGridViewCellEventArgs) _
                                    Handles SongsDataGridView.CellClick

        'set the id = to the row clicked
        intIDClick = SongsDataGridView.Rows(e.RowIndex).Cells(0).Value

    End Sub

    Public Sub play(ByVal intPlay As Integer)           'APB 20150525
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'make sure that a song was selected
        If intPlay < 1 Then
            MsgBox("Please choose a song by selecting it in the table")
            Exit Sub
        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''         stuff for shuffle '''''''''''''''''''''''''''''
        If (chkShuffle.Checked = True) Then
            'get number of songs in db
            Dim intTotal As Integer =
                Aggregate total In MusicDBWorkingDataSet.Songs
                Into Count

            'make a random number out of it
            Randomize()
            Dim intRandom As Integer = Int(Rnd() * intTotal) + 1
            intPlay = intRandom
        End If

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''   end shuffle     '''''''''''''''''''''''''''''''''''''''

        'get the number of seconds of the song
        intSeconds =
            Aggregate seconds In MusicDBWorkingDataSet.Songs
                Where seconds.ID = intPlay
                Select seconds.RuntimeSec Into Sum

        'declare variables
        Dim strSong As String
        Dim strPath As String
        Dim strArtist As String
        Dim strAlbum As String = SongsDataGridView.Rows(intPlay - 1).Cells(3).Value.ToString

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'set value of path
        'Uses the same logic as the upload form to find the project path and
        'determine the file to look for based on the passed in ID

        strSong = SongsDataGridView.Rows(intPlay - 1).Cells(1).Value.ToString
        strPath = AppDomain.CurrentDomain.BaseDirectory
        strPath = strPath.Replace("\bin\Debug\", "\Music\")
        strPath += strSong & ".wav"

        'play song
        My.Computer.Audio.Play(strPath,
        AudioPlayMode.Background)


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'set the back ground of the form to reflect the artist playing
        strArtist = SongsDataGridView.Rows(intPlay - 1).Cells(2).Value.ToString

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''       add information to arrays for printing  ''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        strArtistArray(intArrayAccum) = strArtist
        strAlbumArray(intArrayAccum) = strAlbum
        strSongArray(intArrayAccum) = strSong
        'increment the accumulator one
        intArrayAccum += 1



        'test if the file exitst in the images folder
        'set the path of the photo
        strPath = AppDomain.CurrentDomain.BaseDirectory
        strPath = strPath.Replace("\bin\Debug\", "\Images\")
        strPath += strArtist & ".jpg"

        'test directory for file
        If My.Computer.FileSystem.FileExists(strPath) Then
            Me.BackgroundImage = System.Drawing.Image.FromFile(strPath)
        End If

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'set the now playing lables
        lblSong.Text = strSong
        lblArtist.Text = strArtist
        'start timer
        lblStaticTime.Text = GetTime(intSeconds).ToString

        'set the maximum value of the progress bar = to seconds of song
        pBarRunTime.Maximum = intSeconds
        'start the timer
        Timer1.Start()


    End Sub


    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        'display the search form
        searchForm.Show()

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        'set progress bar to increment by one second

        pBarRunTime.Increment(1)

        'as long as there are seconds left, send it to the function to convert the time, decrement one from seconds
        If (intSeconds > 0) Then
            lblTimePlayed.Text = GetTime(intSeconds).ToString
            intSeconds -= 1
        Else
            'clear the labels, stop the music
            pboStop.Visible = False
            pboPlay.Visible = True
            My.Computer.Audio.Stop()
            lblArtist.Visible = False
            lblSong.Visible = False
            'reset the time labels to empty string
            lblStaticTime.Visible = False
            lblTimePlayed.Visible = False
            'stop the timer, stop the audio, reset the progress bar, clear the labels
            Timer1.Stop()
            pBarRunTime.Value = 0
            'check to see if the repeat button is checked
            If chkRepeat.Checked = True Then
                pboStop.Visible = True
                pboPlay.Visible = False
                play(intIDClick)
            End If
        End If



    End Sub

    Public Function GetTime(intSeconds As Integer) As String
        'formatting the total seconds into minutes and seconds
        Dim iSpan As TimeSpan = TimeSpan.FromSeconds(intSeconds)
        Dim strMin As String
        Dim strSec As String

        strMin = iSpan.Minutes.ToString.PadLeft(2, "0"c)
        strSec = iSpan.Seconds.ToString.PadLeft(2, "0"c)

        Return strMin & ":" & strSec

    End Function

    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        'display the upload form
        upload.Show()
    End Sub



    Private Sub pboPlay_Click(sender As Object, e As EventArgs) Handles pboPlay.Click
        'if play is visible, make it not visible, make stop button visible
        If pboPlay.Visible = True Then
            pboPlay.Visible = False
            pboStop.Visible = True
        End If
        'reset the progress bar
        pBarRunTime.Value = 0
        'send the id into the play function
        play(intIDClick)
    End Sub

    Private Sub pboStop_Click(sender As Object, e As EventArgs) Handles pboStop.Click
        'if stop is visible, make it not visible, make play button visible
        If pboStop.Visible = True Then
            pboStop.Visible = False
            pboPlay.Visible = True
        End If

        'reset the time labels to empty string
        lblStaticTime.Visible = False
        lblTimePlayed.Visible = False
        'stop the timer, stop the audio, reset the progress bar, clear the labels
        Timer1.Stop()
        My.Computer.Audio.Stop()
        pBarRunTime.Value = 0

        lblArtist.Visible = False
        lblSong.Visible = False



    End Sub

    Public Sub updateData(ByVal strSong As String, ByVal strArtist As String, ByVal intIDClick As Integer, ByVal intRunTime As Integer, ByVal strAlbum As String)
        'create new row
        'Dim newRow As MusicDBWorkingDataSet.SongsRow
        'newRow = Me.MusicDBWorkingDataSet.Songs.NewSongsRow()
        'newRow.ID = intIDClick
        'newRow.SongTitle = strSong
        'newRow.Artist = strArtist
        'newRow.Album = strAlbum
        'newRow.HasBeenPlayed = False
        'newRow.RuntimeSec = intRunTime
        'newRow.ArtistWebPage = ""
        'newRow.AlbumArt = ""
        'newRow.NumberOfPlays = 0
        'newRow.RandomShuffle = 0
        'newRow.SongPath = ""

        'add row to table


        'insert New record into dataset
        Try
            SongsTableAdapter.InsertQuery(intIDClick, strSong, strArtist, strAlbum, False, intRunTime,
                                          "", "", 0, 0, "")
            Try
                Validate()
                SongsBindingSource.EndEdit()
                SongsTableAdapter.Update(MusicDBWorkingDataSet.Songs)
                MsgBox("Record Saved")
                Me.SongsTableAdapter.Fill(Me.MusicDBWorkingDataSet.Songs)
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Music Player", MessageBoxButtons.OK,
                            MessageBoxIcon.Error)
            End Try

        Catch es As Exception
            MsgBox("Cannot insert duplicate records")
            MsgBox(es.Message)
        End Try
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click


        Me.Close()
    End Sub
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub HelpToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HelpToolStripMenuItem.Click
        Dim strHelp As String = "You better CALL somebody!!!"

        MessageBox.Show(strHelp, "Hold the door", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        Dim strAbout As String = "I mean, it plays music.  What more do you need to know?"

        MessageBox.Show(strAbout, "That which is dead will never die", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub PrintPreviewToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles PrintPreviewToolStripMenuItem.Click
        'Begin process for print preview
        Me.PrintPreviewDialog1.Document = PrintDocument1
        Me.PrintPreviewDialog1.ShowDialog()
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '''''''''''''''''''     print sub   '''''''''''''''''''''''''''''''''''''''''''''''''
    Private Sub PrintToolStripMenuItem1_Click(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        ' Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage

        'Set X and Y coordinates
        Dim horizontalPrintLoc As Single
        Dim verticalPrintLoc As Single

        horizontalPrintLoc = e.MarginBounds.Left
        verticalPrintLoc = e.MarginBounds.Top

        'create new font object for headings and get line height
        Dim headingPrintFont As New Font("Arial", 14, FontStyle.Bold)
        Dim lineHeight14 As Single
        lineHeight14 = headingPrintFont.GetHeight


        'create new font object for regular print and get line height
        Dim printFont10 As New Font("Arial", 10)

        Dim lineHeight10 As Single
        lineHeight10 = printFont10.GetHeight

        'create a variable to hold the string to be printed
        Dim printLine As String
        Dim printLineArtist As String
        Dim printLineSong As String
        Dim printLineAlbum As String

        ' Print the first page
        ' If pageCount = 1 Then

        'create a line to print
        printLine = "Winter is Coming"

        'print a line
        e.Graphics.DrawString(printLine, headingPrintFont, Brushes.Black, horizontalPrintLoc, verticalPrintLoc)

        'move to the next line before printing
        verticalPrintLoc = verticalPrintLoc + lineHeight14 * 2

        printLine = "The songs that were played: "
        'print a second line
        e.Graphics.DrawString(printLine, printFont10, Brushes.Black, horizontalPrintLoc, verticalPrintLoc)

        '*************************************************************************

        'Print out all songs

        For listIndex As Integer = 0 To strAlbumArray.Length - 1

            'check to see if the array is empty
            If strAlbumArray(listIndex) = String.Empty Then
                Exit For

            Else

                'move to the next line before printing
                verticalPrintLoc = verticalPrintLoc + lineHeight10 * 2

                'get a line from the combobox
                printLineSong = strSongArray(listIndex)
                printLineArtist = strArtistArray(listIndex)
                printLineAlbum = strAlbumArray(listIndex)

                'print song
                e.Graphics.DrawString("Song: " & printLineSong, printFont10, Brushes.Black, horizontalPrintLoc, verticalPrintLoc)
                'skip a line
                verticalPrintLoc = verticalPrintLoc + lineHeight10
                'print artist
                e.Graphics.DrawString("Artist: " & printLineArtist, printFont10, Brushes.Black, horizontalPrintLoc, verticalPrintLoc)
                'skip a line
                verticalPrintLoc = verticalPrintLoc + lineHeight10
                'print album
                e.Graphics.DrawString("Album: " & printLineAlbum, printFont10, Brushes.Black, horizontalPrintLoc, verticalPrintLoc)
                'skip a line
                verticalPrintLoc = verticalPrintLoc + lineHeight10
            End If


        Next

        'Print a selected item from a list
        printLine = "That which is dead will never die"

        'move to the next line before printing
        verticalPrintLoc = verticalPrintLoc + lineHeight10 * 2

        'print a line from the ComboBox
        e.Graphics.DrawString(printLine, headingPrintFont, Brushes.Red, horizontalPrintLoc, verticalPrintLoc)

        'move to the next line before printing
        verticalPrintLoc = verticalPrintLoc + lineHeight14 * 2

        ' End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Print multi-page report
        '1. Change printline message
        printLine = "The term is almost over!!!!! "

        '2. print a page of the same line

        'Do
        '    'move to the next line before printing
        '    verticalPrintLoc = verticalPrintLoc + lineHeight10

        '    'print the line
        '    e.Graphics.DrawString(printLine, printFont10, Brushes.Blue, horizontalPrintLoc, verticalPrintLoc)

        '    'stop at the bottom margin

        'Loop Until verticalPrintLoc >= e.MarginBounds.Bottom

        ''3. Increment page number
        'pageCount += 1

        ''4. Indicate if additional pages
        'If pageCount <= 3 Then
        '    e.HasMorePages = True  'makes a recursive call to this subprocedure: PrintDocument1_PrintPage
        'Else
        '    e.HasMorePages = False
        '    'reset the counter for next report
        '    pageCount = 1
        'End If

    End Sub
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '''''''''''''''     end print       ''''''''''''''''''''''''''''''''''''''''''''''''
    Private Sub PrintPreviewDialog1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles PrintPreviewDialog1.Load

        'to maximize the Print Preview on startup:
        Me.PrintPreviewDialog1.WindowState = FormWindowState.Maximized



    End Sub
    Private Sub pboForward_Click(sender As Object, e As EventArgs) Handles pboForward.Click
        'stop the music
        My.Computer.Audio.Stop()
        'get the total number of songs in the db
        Dim intTotal As Integer =
            Aggregate total In MusicDBWorkingDataSet.Songs
            Into Count

        'if it's at the last song, go to song 1
        'else go to the next song
        If (intIDClick = intTotal) Then
            intIDClick = 1
        Else
            intIDClick += 1
        End If
        pBarRunTime.Value = 0
        'send the id to the play function
        play(intIDClick)
    End Sub

    Private Sub pboRwd_Click(sender As Object, e As EventArgs) Handles pboRwd.Click
        'stop the music
        My.Computer.Audio.Stop()
        'get the total number of songs in the db
        Dim intTotal As Integer =
            Aggregate total In MusicDBWorkingDataSet.Songs
            Into Count

        'if it's at the first song, go to the last song
        'else go to the previous song
        If (intIDClick = 1) Then
            intIDClick = intTotal
        Else
            intIDClick -= 1
        End If
        pBarRunTime.Value = 0
        'send the id to the play function
        play(intIDClick)
    End Sub

    Private Sub chkRepeat_CheckedChanged(sender As Object, e As EventArgs) Handles chkRepeat.CheckedChanged
        My.Computer.Audio.Stop()
        'make sure that shuffle and repeat are not both checked
        If (chkShuffle.Checked = True And chkRepeat.Checked = True) Then
            MessageBox.Show("It doesn't make sense to have repeat and shuffle both checked",
                            "You know nothing, Jon Snow", MessageBoxButtons.OK, MessageBoxIcon.Information)
            chkRepeat.Checked = False
        Else
            'make the label visible or not
            If chkRepeat.Checked = True Then
                lblRepeat.Visible = True
            Else
                lblRepeat.Visible = False

            End If
        End If
    End Sub

    Private Sub chkShuffle_CheckedChanged(sender As Object, e As EventArgs) Handles chkShuffle.CheckedChanged
        My.Computer.Audio.Stop()
        'make sure that shuffle and repeat are not both checked
        If (chkRepeat.Checked = True And chkShuffle.Checked = True) Then
            MessageBox.Show("It doesn't make sense to have shuffle and repeat both checked",
                            "You know nothing, Jon Snow", MessageBoxButtons.OK, MessageBoxIcon.Information)
            chkShuffle.Checked = False
        Else
            'make the label visible or not
            If chkShuffle.Checked = True Then
                lblShuffle.Visible = True
            Else
                lblShuffle.Visible = False
            End If
        End If
    End Sub

End Class
